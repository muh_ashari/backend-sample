package com.nostratech.sample.converter;

import com.nostratech.sample.persistence.domain.Sample;
import com.nostratech.sample.util.ExtendedSpringBeanUtil;
import com.nostratech.sample.vo.SampleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class SampleVoConverter implements IBaseVoConverter<SampleVO, Sample> {

    @Autowired
    private BaseVoConverter baseVoConverter;

    /**
     * transfer value from vo object to domain object
     * for enum value, please do manually using Enum.values()[ordinal]
     *
     * @param vo
     * @param model
     * @return
     */
    @Override
    public Sample transferVOToModel(SampleVO vo, Sample model) {
        if (null == model) model = new Sample();

        baseVoConverter.transferVOToModel(vo, model);

        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"name", "description", "address", "capacity"});

        return model;
    }

    /**
     * transfer value from list of domain object to list of vo object
     *
     * @param models
     * @param vos
     * @return
     */
    @Override
    public Collection<SampleVO> transferListOfModelToListOfVO(Collection<Sample> models, Collection<SampleVO> vos) {

        if (null == vos) vos = new ArrayList<SampleVO>();

        if (null == models) return vos;

        for (Sample model : models) {
            SampleVO vo = new SampleVO();
            transferModelToVO(model, vo);
            vos.add(vo);
        }
        return vos;
    }

    /**
     * transfer value from domain object to vo object
     *
     * @param model
     * @param vo
     * @return
     */
    @Override
    public SampleVO transferModelToVO(Sample model, SampleVO vo) {
        if (null == vo) vo = new SampleVO();

        baseVoConverter.transferModelToVO(model, vo);

        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"name", "description", "address", "capacity"});

        return vo;
    }
}
