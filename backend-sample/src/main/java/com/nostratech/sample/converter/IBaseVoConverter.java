package com.nostratech.sample.converter;

import java.util.Collection;

public interface IBaseVoConverter<V, T> {

    /**
     * transfer value from vo object to domain object
     * for enum value, please do manually using Enum.values()[ordinal]
     * @param vo
     * @param model
     * @return
     */
    public T transferVOToModel(V vo, T model);

    /**
     * transfer value from list of domain object to list of vo object
     * @param models
     * @param vos
     * @return
     */
    public Collection<V> transferListOfModelToListOfVO(Collection<T> models, Collection<V> vos);

    /**
     * transfer value from domain object to vo object
     * @param model
     * @param vo
     * @return
     */
    public V transferModelToVO(T model, V vo);
}