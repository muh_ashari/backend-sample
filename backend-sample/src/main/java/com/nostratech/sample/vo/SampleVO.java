package com.nostratech.sample.vo;

import java.math.BigDecimal;

public class SampleVO extends BaseVO {

    private String name;
    private String description;
    private String address;
    private BigDecimal capacity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getCapacity() {
        return capacity;
    }

    public void setCapacity(BigDecimal capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SampleVO{");
        sb.append("name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append(", capacity=").append(capacity);
        sb.append('}');
        return sb.toString();
    }
}
