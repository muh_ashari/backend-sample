package com.nostratech.sample.rest;

import com.nostratech.sample.exception.ServiceException;
import com.nostratech.sample.util.RestUtil;
import com.nostratech.sample.util.StatusCode;
import com.nostratech.sample.vo.ResultVO;
import org.springframework.http.ResponseEntity;

public abstract class AbstractBaseController {

    protected static abstract class AbstractRequestHandler {

        public ResponseEntity<ResultVO> getResult() {
            ResultVO result = new ResultVO();
            try {
                Object obj = processRequest();
                if (obj != null) {
                    result.setMessage(StatusCode.OK.name());
                    result.setResult(obj);
                }
            } catch (ServiceException e) {
                result.setMessage(e.getCode().name());
                result.setResult(e.getMessage());
            }
            return RestUtil.getJsonResponse(result);
        }

        public abstract Object processRequest();

    }

}
