package com.nostratech.sample.persistence.repository;

import com.nostratech.sample.persistence.domain.Sample;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SampleRepository extends JpaRepository<Sample, Integer> {

    public Sample findBySecureId(String secureId);
    
}
