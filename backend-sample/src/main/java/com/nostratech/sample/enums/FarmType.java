package com.nostratech.sample.enums;

public enum FarmType implements BaseModelEnum<String> {
    WICK("Wick"),
    DRIP("Drip"),
    EBBFlLOW("Ebb And Flow"),
    WATER("WATER"),
    CULTURE("Culture"),
    NFT("NFT"),
    AEROPONICS("Aeroponics"),
    HYDROPONICS("Hydroponics");


    private String internalValue;

    FarmType(String internalValue) {
        this.internalValue = internalValue;
    }

    @Override
    public String getInternalValue() {
        return internalValue;
    }
}
